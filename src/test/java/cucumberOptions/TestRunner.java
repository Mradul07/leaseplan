package cucumberOptions;


import org.junit.Test;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/*
 * Description :
 * This class is the base class which will be triggered to run all the Scenarios in the feature file
 * The class contains the mapping with the feature file under @CucumberOptions annotation and glue with the
 * implementation of the keywords in Scenario in stepDefinition class. It used the pretty plugin to generate the Cucumber test report
 * The test report can be found in the "target" folder under the project folder directly
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/BooksAPI.feature", glue = "stepDefinition", plugin = { "pretty", "html:target/cucumber-reports" })
public class TestRunner {

}