Feature: Books API

  Scenario: Verify authentication
    Given user tries to access the resource without access key
    When  user makes a GET request
    Then  the API call is unsuccessful with status code 401
    And error code as "steps.oauth.v2.FailedToResolveAPIKey"


  Scenario: Verify user recieves error when on an incorrect request made
    Given user tries to access the resource
    When  user makes a GET request
    Then  the API call is unsuccessful with status code 400
    And response status is "ERROR"


  Scenario Outline: Verify user tries to retrieve the best sellers list
    Given user tries to access the resource
    When  user makes a GET request with "<date>" and "<list_name>"
    Then  the API call is successful with status code 200
    And response status is "OK"

    Examples:
      | date       | list_name         |
      | 2008-06-08 | hardcover-fiction |
