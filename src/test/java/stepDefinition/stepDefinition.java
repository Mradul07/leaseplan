package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.*;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/*
 * Description :
 * This class contains the methods to perform the actions mentioned in the feature file.
 * The actions under the Given, When, & Then keywords, in the all the Scenarios have their implementation defined in this class
 */

public class stepDefinition extends resources.Utilities {
    RequestSpecification res;
    Response response;


    @Given("user tries to access the resource without access key")
    public void user_tries_to_access_the_resource_without_access_key() throws IOException {
        res = given().spec(requestSpecification());
    }

    @Given("user tries to access the resource")
    public void user_tries_to_access_the_resource() throws IOException {
        res = given().spec(requestSpecification()).queryParam("api-key", "PID06a9Qd6r2yqptV7Nl0cmo8xRj4QLv");
    }

    @When("user makes a GET request")
    public void user_makes_a_get_request() {
        response = res.when().get(".json");
    }

    @When("user makes a GET request with {string} and {string}")
    public void user_makes_a_get_request_with_and(String date, String list_name) {
        response = res.when().get("/" + date + "/" + list_name + ".json");
    }

    @Then("the API call is unsuccessful with status code {int}")
    public void the_api_call_is_unsuccessful_with_status_code(Integer int1) {
        res.then().log().all().statusCode(int1);
    }

    @Then("the API call is successful with status code {int}")
    public void the_api_call_is_successful_with_status_code(Integer int1) {
        res.then().log().all().statusCode(int1);
    }

    @Then("error code as {string}")
    public void error_code_as(String errorCode_expected) {
        assertEquals(response.getStatusCode(), 401);
        String errorCode_actual = getJsonPath(response, "fault.detail.errorcode");
        assertEquals(errorCode_actual, errorCode_expected);
    }

    @Then("response status is {string}")
    public void response_status_is(String responseStatus_expected) {
        if (responseStatus_expected.equals("ERROR")) {
            assertEquals(response.getStatusCode(), 400);
        } else if (responseStatus_expected.equals("OK")) {
            assertEquals(response.getStatusCode(), 200);
        }
        String responseStatus_actual = getJsonPath(response, "status");
        assertEquals(responseStatus_actual, responseStatus_expected);
    }
}
