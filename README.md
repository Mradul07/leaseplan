# LeasePlan-Assignment

## Table of Contents
- [Items in repository](#items-in-repository)
- [Description of the project](#description-of-the-project)
- [Pre-requisites to run each project](#pre-requisites-to-run-each-project)
- [Execution](#execution)
- [Verification](#verification)
- [Contact](#contact)

## Items in repository 
  1. **LeasePlanAssignment** - this project contains
     - Test cases for BooksAPI (documentation https://developer.nytimes.com/docs/books-product/1/overview)
  2. **BooksAPI Manual test cases.postman_collection** This is a postman collection for manual test cases to be run for **BooksAPI** 
  
  
## Description of the project
1. **LeasePlanAssignment**
   - This is a Maven project, hence all the dependencies and pluggins supporting the Cucumber & Rest Assured are present in the POM.xml
   - The project structure contains 
      - the directory "src/test/java", where the test cases based on Cucumber framework are located to test the external BooksAPI 

## Pre-requisites to run each project
  - Windows or Mac pc
  - Java8 or higher pre-installed
  - Maven pre-installed (so that all the dependencies can be autoinstalled at the time of execution)
  - (Optional) If you wish to run the project in an IDE then there are various options like Eclipse, Intellij, etc. The projects are build using IntelliJ.
  - (Optional) Cucumber pluggin if using IDE, to see the feature files
## Execution
  - Download or Clone the project from Git repository
  - Steps to run from cmd line 
      - Navigate to the project directory (make sure you obly navigate until parent project directory where pom.xml is present)
      - Command to run the cucumber test cases   -> **mvn test** (this will trigger the test cases in the feature file)
  - Steps to run from IDE
    - Download or clone the project in IDE
    - Navigate to the feature file - "src/test/java/features/BookingAPI.feature"
    - Run the entire featue file or individual test case (Note : The detele & partial update test cases are dependent on authentication test case to get the access       token and hence can only be run in combination with the authentication test case (1st in feature file))      
    
## Verification
  The verification can be done by looking into the log files, console/terminal output & Cucumber test reports. The location of the log file and report are below:
  1. log file - LeasePlanAssignment/logging.txt. the log file shows all the request/response created per scenario/test case
  2. Cucumber report - LeasePlanAssignment/target/cucumber-reports (This is an HTML report, if by chance it doesn't open in the broswer change the extension to .html to see the report).The report shows the status of all the scenarios/test cases in the feature file in the BDDish language
  
## Contact
If you have any questions related to the project feel free to reach out to me on mradul.s2012@gmail.com. I'll do my best to revert asap.
Good luck reviewing :)
